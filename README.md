[![Quality Gate Status](https://sonar.digitalservice.id/api/project_badges/measure?project=sapawarga-webadmin&metric=alert_status)](https://sonar.digitalservice.id/dashboard?id=sapawarga-webadmin)
[![Maintainability Rating](https://sonar.digitalservice.id/api/project_badges/measure?project=sapawarga-webadmin&metric=sqale_rating)](https://sonar.digitalservice.id/dashboard?id=sapawarga-webadmin)
[![Reliability Rating](https://sonar.digitalservice.id/api/project_badges/measure?project=sapawarga-webadmin&metric=reliability_rating)](https://sonar.digitalservice.id/dashboard?id=sapawarga-webadmin)
[![Security Rating](https://sonar.digitalservice.id/api/project_badges/measure?project=sapawarga-webadmin&metric=security_rating)](https://sonar.digitalservice.id/dashboard?id=sapawarga-webadmin)

# Sapawarga Admin Web
Sapawarga Admin Web application is currently intended for tiered government admins.

## Tech Stack
- Vue CLI - https://cli.vuejs.org/
- Element UI - https://element.eleme.io/#/en-US  (for Vue 2.x version)

## Installation and setup product
1. Clone this project
```
# with SSH
git@gitlab.com:jdsteam/sapa-warga/sapawarga-webadmin.git

# with HTTPS
https://gitlab.com/jdsteam/sapa-warga/sapawarga-webadmin.git
```
2. Create .env file and set environment variables (check .env-template for reference)
3. Install Dependencies **(using YARN)**
```
yarn install
```
4. Serve with hot reload (default at localhost:3000)
```
yarn dev
```
