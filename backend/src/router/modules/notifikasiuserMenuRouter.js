import Layout from '@/layout'
import popupInformasiRouter from './ListMenu/popupInformasiRouter'
import bannerRouter from './ListMenu/bannerRouter'
import notificationRouter from './ListMenu/notificationRouter'

const notifikasiuserMenuRouter = {
  name: 'notification-user',
  path: 'notification-user',
  component: Layout,
  redirect: '/',
  meta: {
    title: 'notification-user',
    roles: ['admin', 'staffProv'],
    icon: 'guide'
  },
  active: false,
  children: [
    ...bannerRouter,
    ...notificationRouter,
    ...popupInformasiRouter
  ]
}

export default notifikasiuserMenuRouter
