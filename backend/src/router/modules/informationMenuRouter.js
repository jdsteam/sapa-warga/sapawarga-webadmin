import Layout from '@/layout'
import nomorPentingRouter from './ListMenu/nomorPentingRouter'
import newsRouter from './ListMenu/newsRouter'
import newsHoaxRouter from './ListMenu/newsHoaxRouter'
import videoRouter from './ListMenu/videoRouter'
import newsImportantRouter from './ListMenu/newsImportantRouter'

const informationMenuRouter = {
  name: 'information',
  path: 'information',
  component: Layout,
  redirect: 'noredirect',
  meta: {
    title: 'information',
    roles: ['admin', 'staffProv', 'staffSaberhoax', 'staffKabkota', 'staffKec', 'staffKel', 'staffOPD'],
    icon: 'news'
  },
  active: false,
  children: [
    ...newsRouter,
    ...newsImportantRouter,
    ...newsHoaxRouter,
    ...nomorPentingRouter,
    ...videoRouter
  ]
}

export default informationMenuRouter
