import Layout from '@/layout'
import aspirationRouter from './ListMenu/aspirationRouter'
import qnaRouter from './ListMenu/qnaRouter'
import postRouter from './ListMenu/postRouter'

const aspirationMenuRouter = {
  name: 'aspiration',
  path: 'aspiration',
  component: Layout,
  redirect: '/',
  meta: {
    title: 'aspiration',
    roles: ['admin', 'staffProv', 'staffKabkota', 'staffKec', 'staffKel', 'pimpinan'],
    icon: 'aspiration'
  },
  active: false,
  children: [
    ...postRouter,
    ...qnaRouter,
    ...aspirationRouter
  ]
}

export default aspirationMenuRouter
