import Layout from '@/layout'
import categoriesRouter from './ListMenu/categoriesRouter'
import newsChannelRouter from './ListMenu/newsChannelRouter'

const configurationMenuRouter = {
  name: 'configuration-menu',
  path: 'configuration-menu',
  component: Layout,
  redirect: '/',
  meta: {
    title: 'configuration-menu',
    roles: ['admin', 'staffProv'],
    icon: 'tab'
  },
  active: false,
  children: [
    ...categoriesRouter,
    ...newsChannelRouter
  ]
}

export default configurationMenuRouter
