import Vue from 'vue'
import Router from 'vue-router'
const pkg = require('../../package.json')

/* Sentry */
import * as Sentry from '@sentry/core'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/* Router Modules */
import dashboardRouter from './modules/dashboardRouter'
import userRouter from './modules/userRouter'
import releaseManagementRouter from './modules/releaseManagementRouter'
import gamificationRouter from './modules/gamificationRouter'
import informationMenuRouter from './modules/informationMenuRouter'
import aspirationMenuRouter from './modules/aspirationMenuRouter'
import pollingsurveyMenuRouter from './modules/pollingsurveyMenuRouter'
import notifikasiuserMenuRouter from './modules/notifikasiuserMenuRouter'
import configurationMenuRouter from './modules/configurationMenuRouter'
import broadcastRouter from './modules/broadcastRouter'

/* Router Modules for Leader */
import aspirationLeaderMenuRouter from './modules/LeaderMenu/aspirationLeaderMenuRouter'
import pollingLeaderMenuRouter from './modules/LeaderMenu/pollingLeaderMenuRouter'
import surveyLeaderMenuRouter from './modules/LeaderMenu/surveyLeaderMenuRouter'
import newsLeaderMenuRouter from './modules/LeaderMenu/newsLeaderMenuRouter'
import newsHoaxLeaderMenuRouter from './modules/LeaderMenu/newsHoaxLeaderMenuRouter'
import qnaLeaderMenuRouter from './modules/LeaderMenu/qnaLeaderMenuRouter'
// import { KAB_KOTA_ACCESS_BNBA } from '@/utils/constantVariable'

/** note: sub-menu only appear when children.length>=1
 *  detail see  https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 **/

/**
* hidden: true                   if `hidden:true` will not show in the sidebar(default is false)
* alwaysShow: true               if set true, will always show the root menu, whatever its child routes length
*                                if not set alwaysShow, only more than one route under the children
*                                it will becomes nested mode, otherwise not show the root menu
* redirect: noredirect           if `redirect:noredirect` will no redirect in the breadcrumb
* name:'router-name'             the name is used by <keep-alive> (must set!!!)
* meta : {
    roles: ['admin','editor']    will control the page roles (you can set multiple roles)
    title: 'title'               the name show in sub-menu and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    noCache: true                if true, the page will no be cached(default is false)
    breadcrumb: false            if false, the item will hidden in breadcrumb(default is true)
    affix: true                  if true, the tag will affix in the tags-view
  }
**/

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 * */
export const constantRoutes = [
  {
    path: '/redirect',
    component: Layout,
    hidden: true,
    active: false,
    children: [
      {
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/landing-page',
    component: () => import('@/views/landingPage/index'),
    active: false,
    hidden: true
  },
  {
    path: '/privacy-policy',
    name: 'privacy-policy',
    component: () => import('@/views/landingPage/term'),
    active: false,
    hidden: true
  },
  {
    path: '/term-of-service',
    name: 'term-of-service',
    component: () => import('@/views/landingPage/term'),
    active: false,
    hidden: true
  },
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    active: false,
    hidden: true
  },
  {
    title: 'reset-password',
    path: '/reset-password',
    component: () => import('@/views/resetPassword/index'),
    active: false,
    hidden: true
  },
  {
    path: '/info-penting',
    active: false,
    component: () => import('@/views/public/index'),
    hidden: true
  },
  {
    path: '/auth-redirect',
    component: () => import('@/views/login/authredirect'),
    active: false,
    hidden: true
  },
  {
    title: 'verifikasi-user',
    path: '/confirm',
    component: () => import('@/views/verifikasiUser/index'),
    active: false,
    hidden: true
  },
  {
    path: '/403',
    component: () => import('@/views/errorPage/403'),
    active: false,
    hidden: true
  },
  {
    path: '/404',
    component: () => import('@/views/errorPage/404'),
    active: false,
    hidden: true
  },
  {
    path: '',
    component: Layout,
    redirect: 'profile',
    active: false,
    children: [
      {
        name: 'Profile',
        path: 'profile',
        component: () => import('@/views/profile/index'),
        hidden: true,
        meta: {
          title: 'profile',
          roles: ['admin', 'staffProv', 'staffSaberhoax', 'staffKabkota', 'staffKec', 'staffKel']
        }
      }
    ]
  },
  {
    path: '/edit-profile',
    component: Layout,
    active: false,
    children: [
      {
        name: 'profile-edit',
        path: '',
        component: () => import('@/views/profile/edit'),
        hidden: true,
        meta: {
          title: 'profile',
          roles: ['admin', 'staffProv', 'staffSaberhoax', 'staffKabkota', 'staffKec', 'staffKel']
        }
      }
    ]
  }
]

/**
 * asyncRoutes
 * the routes that need to be dynamically loaded based on user roles
 */
export const asyncRoutes = [
  // title
  {
    name: 'KELOLA SAPAWARGA',
    // add object 'action' to disable click & path must be unique
    action: false,
    path: 'no-route-title-sapawarga',
    component: Layout,
    redirect: 'noredirect',
    meta: {
      title: 'KELOLA SAPAWARGA',
      roles: ['admin', 'staffProv', 'staffKabkota', 'staffKec', 'staffKel']
    },
    active: false
  },
  dashboardRouter,
  informationMenuRouter,
  pollingsurveyMenuRouter,
  aspirationMenuRouter,
  notifikasiuserMenuRouter,
  broadcastRouter,
  userRouter,
  configurationMenuRouter,
  releaseManagementRouter,
  gamificationRouter,
  { path: '*', redirect: '/404', active: false, hidden: true }
]

export const leaderRoutes = [
  dashboardRouter,
  qnaLeaderMenuRouter,
  aspirationLeaderMenuRouter,
  pollingLeaderMenuRouter,
  surveyLeaderMenuRouter,
  newsLeaderMenuRouter,
  newsHoaxLeaderMenuRouter,
  { path: '*', redirect: '/404', active: false, hidden: true }
]

function createRouter() {
  const router = new Router({
  // mode: 'history', // require service support
    scrollBehavior: () => ({ y: 0 }),
    routes: constantRoutes
  })

  router.beforeEach(beforeEach)

  return router
}

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

async function beforeEach(to, from, next) {
  Sentry.configureScope((scope) => {
    scope.setTransactionName(to.path)
  })

  const nearestWithTitle = to.matched.slice().reverse().find(r => r.path)

  if (nearestWithTitle !== undefined) {
    const taged = nearestWithTitle.path.split('/')
    // Remove any stale meta tags from the document using the key attribute we set below.
    Array.from(document.querySelectorAll('[data-vue-router-controlled]')).forEach(el => el.parentNode.removeChild(el))

    // Turn the meta tag definitions into actual elements in the head.
    if ((taged[1] === 'reset-password') || (taged[1] === 'confirm')) {
      // If a route with a title was found, set the document (page) title to that value.
      if (nearestWithTitle) document.title = taged[1]
    } else {
      document.title = pkg.name
    }
  }
  next()
}

export default router
