import moment from 'moment'
const locale = 'id'
moment.locale(locale)
require('moment-timezone')

export function parsingDatetime(datetime, format) {
  if (format) {
    datetime = moment(datetime * 1000).format(format)
  } else {
    datetime = moment(datetime * 1000).format('DD MMMM YYYY HH:mm')
  }
  return datetime
}

export function formatDatetime(datetime, format) {
  datetime = moment.utc(datetime).tz('Asia/Jakarta').format(format)
  return datetime
}

export function formatDatetimeUTC(datetime, format) {
  if (datetime) {
    datetime = moment.utc(datetime).tz('Asia/Jakarta')
    const formattedDateTime = moment(datetime._d).format(format)
    return formattedDateTime
  } else {
    return '-'
  }
}
