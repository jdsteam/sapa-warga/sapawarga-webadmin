export const faqs = [
  {
    id: 1,
    head: 'Siapa saja pengguna Sapawarga?',
    body: 'Saat ini, Sapawarga diperuntukan bagi Ketua RW dan Pendamping Lokal Desa (PLD). Namun kami sedang melakukan inovasi dan pengembangan aplikasi agar nantinya Sapawarga dapat digunakan oleh seluruh warga Jabar.'
  },
  {
    id: 2,
    head: 'Bagaimana cara mendapatkan akun Sapawarga?',
    body: 'Saat ini RW bisa meminta akun Sapawarga dengan menghubungi WhatsApp Hotline Sapawarga di nomor 0812-2285-3255.'
  },
  {
    id: 3,
    head: 'Apakah aplikasi ini hanya untuk Ketua RW dan belum bisa digunakan untuk masyarakat umum/publik?',
    body: 'Aplikasi Sapawarga untuk publik sedang kami kembangkan, saat ini masih dalam tahap uji coba. Jika kamu berminat untuk menjadi bagian dari pengembangan, kamu bisa klik tombol “Isi Formulir” untuk menjadi 300 orang pertama yang mencoba aplikasi Sapawarga versi publik.'
  },
  {
    id: 4,
    head: 'Apa keuntungan mengikuti uji coba aplikasi?',
    body: 'Mengikuti uji coba aplikasi berarti menjadi bagian dari pengembangan aplikasi sebelum resmi rilis ke publik. Kamu bisa mencoba berbagai fitur dan memberikan feedback untuk pengembangan.'
  },
  {
    id: 5,
    head: 'Apa yang harus dilakukan jika terdapat masalah dalam mengakses akun?',
    body: 'Hubungi Hotline Sapawarga melalui WhatsApp di nomor 0812-2285-3255.'
  }
]
