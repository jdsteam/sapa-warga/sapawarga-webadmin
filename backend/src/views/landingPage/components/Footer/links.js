export const links = [
  {
    id: 'panduan',
    name: 'Buku Panduan Sapawarga',
    url: 'http://bit.ly/SWbukupanduan3'
  },
  {
    id: 'pjb',
    name: 'Portal Jabar',
    url: 'https://jabarprov.go.id/'
  },
  {
    id: 'jds',
    name: 'Jabar Digital Service',
    url: 'https://digitalservice.jabarprov.go.id/'
  },
  {
    id: 'edj',
    name: 'Integrated Portal Data Jabar',
    url: 'https://data.jabarprov.go.id/'
  }
]
