export const features = [
  {
    background: 'bg-blue-900',
    title: 'WADAH PENYAMPAIAN ASPIRASI',
    subtitle: 'Sampaikan suara dan bantu membangun Jabar melalui:',
    content: [
      {
        icon: require('@/assets/landing-page/icon-fitur-survey.svg'),
        text: 'Fitur Survey'
      },
      {
        icon: require('@/assets/landing-page/icon-kegiatan-rw.svg'),
        text: 'Kegiatan RW'
      }
    ]
  },
  {
    background: 'bg-green-900',
    title: 'MEDIA INFORMASI DAN KOMUNIKASI',
    subtitle: 'Dapatkan informasi yang akurat dengan mengakses:',
    content: [
      {
        icon: require('@/assets/landing-page/icon-info-penting.svg'),
        text: 'Info Penting'
      },
      {
        icon: require('@/assets/landing-page/icon-jabar-saber-hoax.svg'),
        text: 'Jabar Saber Hoax'
      },
      {
        icon: require('@/assets/landing-page/icon-berita.svg'),
        text: 'Berita'
      }
    ]
  },
  {
    background: 'bg-pink-900',
    title: 'DIGITALISASI LAYANAN PUBLIK',
    subtitle: 'Akses layanan publik kini bisa dilakukan dengan cepat dan dimana saja lewat:',
    content: [
      {
        icon: require('@/assets/landing-page/icon-administrasi.svg'),
        text: 'Administrasi'
      },
      {
        icon: require('@/assets/landing-page/icon-pajak.svg'),
        text: 'Pajak Kendaraan Bermotor'
      }
    ]
  }
]
