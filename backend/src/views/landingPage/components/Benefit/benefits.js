export const benefits = [
  {
    title: 'Keterbukaan Informasi',
    subtitle: 'Mendapatkan Informasi akurat dan terhindar dari berita hoaks karena informasi di Sapawarga telah tervalidasi.'
  },
  {
    title: 'Keterlibatan secara Langsung dalam Pembangunan Jawa Barat',
    subtitle: 'Ikut berpartisipasi dengan menyampaikan aspirasi langsung kepada Pemerintah Daerah Provinsi Jawa Barat.'
  },
  {
    title: 'Efisiensi Layanan Publik',
    subtitle: 'Akses Layanan administrasi publik kini tak perlu lagi melewati tahapan yang rumit.'
  },
  {
    title: 'Mengoptimalkan Kembali Peran RW',
    subtitle: 'Menggerakkan partisipasi masyarakat dalam pembangunan dengan bertukar informasi.'
  },
  {
    title: 'Meningkatkan Pengalaman Digital',
    subtitle: 'Pemberian smartphone yang diimbangi dengan pelatihan iterasi digital untuk bantu pengguna dalam beradaptasi.'
  }
]
