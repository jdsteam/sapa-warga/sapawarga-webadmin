export const testimonials = [
  {
    id: 1,
    name: 'Sofyan Fatah',
    role: 'Ketua RW',
    img: '',
    text: 'Ini banyak membantu kalau liat isinya (fitur). Bisa bayar kewajiban-kewajiban seperti kendaraan, PBB, dan sebagainya. Selain itu, bisa komunikasi langsung sama pemerintah juga.'
  },
  {
    id: 2,
    name: 'Dede Nurlaeni',
    role: 'Pendamping Lokal Desa',
    img: '',
    text: 'Terima kasih kepada jajaran Pemerintah Jawa Barat, khususnya Bapak Gubernur Ridwan Kamil. Mudah-mudahan beliau bisa memajukan masyarakat Jawa Barat bersama kami melalui aplikasi Sapawarga ini.'
  },
  {
    id: 3,
    name: 'Dadang Setiawan',
    role: 'Ketua RW',
    img: '',
    text: 'Terima kasih kepada Pemerintah Daerah Provinsi Jawa Barat yang telah memberikan inovasi bagi masyarakat Jawa Barat. Semoga kedepan bisa mewujudkan Jabar Juara Lahir Batin. Jabar Juara!'
  }
]
