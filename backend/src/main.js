import Vue from 'vue'

import moment from 'moment'
moment.locale('id')

import VueMoment from 'vue-moment'
import VueTheMask from 'vue-the-mask'
import InfiniteScroll from 'vue-infinite-scroll'
import L from 'leaflet'
import VueAnalytics from 'vue-analytics'
import Cookies from 'js-cookie'

import 'normalize.css/normalize.css' // A modern alternative to CSS resets

import Element from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'

import '@jabardigitalservice/jds-design-system/css/jds-design-system.css'
import { DesignSystem } from '@jabardigitalservice/jds-design-system'

import '@/styles/index.scss' // global css
import '@/styles/main.scss' // costume css

import _ from 'lodash'
Object.defineProperty(Vue.prototype, '_', { value: _ })

import App from './App'
import store from './store'
import router from './router'

// import './registerServiceWorker' // init PWA / register Service Worker

import i18n from './lang' // Internationalization
import './icons' // icon
import './permission' // permission control
import './utils/errorLog' // error log

import * as filters from './filters' // global filters

import { mockXHR } from '../mock' // simulation data

import Sentry from './plugins/sentry' // sentry

import VueCurrencyFilter from 'vue-currency-filter'

// mock api in github pages site build
if (process.env.VUE_APP_NODE_ENV === 'production') { mockXHR() }

Vue.use(Sentry)

Vue.use(DesignSystem)
Vue.use(Element, {
  size: Cookies.get('size') || 'medium', // set element-ui default size
  i18n: (key, value) => i18n.t(key, value)
})

Vue.use(VueMoment, {
  moment
})

Vue.use(VueCurrencyFilter, {
  symbol: '',
  thousandsSeparator: '.',
  fractionCount: 0,
  fractionSeparator: ',',
  symbolPosition: 'front',
  symbolSpacing: true
})

Vue.use(VueTheMask)
Vue.use(InfiniteScroll)
Vue.use(L)

// analytic
Vue.use(VueAnalytics, {
  id: process.env.VUE_APP_ANALYTICS_ID,
  router
})

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

Vue.config.productionTip = false

// unregister service worker
navigator.serviceWorker.getRegistrations().then(function(registrations) {
  for (const registration of registrations) {
    registration.unregister()
  }
})

new Vue({
  el: '#app',
  router,
  store,
  i18n,
  render: h => h(App)
})
