import * as Sentry from '@sentry/vue'
import { BrowserTracing } from '@sentry/tracing/esm/browser'
import store from '../store'

export default {
  install(Vue) {
    Sentry.init({
      Vue,
      release: process.env.VUE_APP_VERSION,
      environment: process.env.VUE_APP_ERROR_ENVIRONMENT,
      dsn: process.env.VUE_APP_SENTRY_DSN,
      integrations: [
        new BrowserTracing()
      ],
      tracesSampleRate: parseFloat(process.env.VUE_APP_SENTRY_TRACE_SAMPLE_RATE),
      tracingOptions: {
        trackComponents: true
      }
    })
    Sentry.configureScope((scope) => {
      scope.setTag('User', '')
      scope.setUser({
        id: parseInt(store.getters.user_id)
      })
    })
  }
}
